# 080Cell

無限空間に分子がうかんだクラスタ系のプログラムをこれまで書いてきましたが、
容器のなかにとじこめられた状態を実現するために、周期境界条件を満足する直方体の
シミュレーションボックスを導入します。

とはいっても、変更点はほんのわずかです。
このわずかな変更で、なぜ周期境界条件を
実現できるのか、じっくり考えてみて下さい。


