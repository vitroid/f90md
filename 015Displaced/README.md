# 015Displaced

前節で用いた方程式は、2つの粒子間の平衡（自然）距離がゼロであることを前提としています。 しかし、現実のバネの平衡長はゼロではなく、有限の値です。 そこで、より現実的なバネに対応した式に修正しましょう。 この考え方は、分子内共有結合を有限の自然長を持つバネで表現する場合にも有効です。

二つの粒子が自然長$`L`$にあるとき、相互作用ポテンシャルは最小となる。
```math
\begin{equation}
    V({\bf r}_1, {\bf r}_2) = \frac{k}{2}\left(\left|{\bf r}_1-{\bf r}_2\right| - L\right)^2.
\end{equation}
```

前節と同様に、粒子の位置で方程式を微分することで力を得ることができます。
```math
\begin{equation}
    \begin{split}
        {\bf F}_1 = -\frac{\partial V}{\partial{\bf r}_1}\\
        {\bf F}_2 = -\frac{\partial V}{\partial{\bf r}_2}
    \end{split}
\end{equation}
```

## 実践編 015-1

上記の微分を行い、力の方程式を導出せよ。

