# 100Modular

だんだんプログラムが長くなってきました。(文章を書くのも面倒になってきました)
コメントをたくさん書き込み、できるだけ詳しい説明を入れながらプログラムを書いていても、プログラム全体がパソコンの画面におさまらない長さになってくると、全体像を理解するのが難しくなってきます。ざっと目を遠すには、1つの処理は40〜60行程度(プリンタ用紙1枚程度)におさめるのがよさそうです。

そこで、プログラムを小さい部品に分割します。個々の部品の機能を明確にしておけば、
部分をみて全体を把握しやすくなります。

... fortranの`module`機能を使います。

## `module`を使う場合の注意点

### `module`は構造体でもオブジェクトでもない。

モジュールは単にサブルーチンを束ねたものです．それ以上でもそれ以下でもありません．モジュールには変数が含まれますが，構造体やクラスのようにコピーを作ることはできず，モジュール内変数は単なるグローバル変数と違いがありません．例えば「分子」というモジュールを1つ作って，いろんな種類の分子を派生させる，というような使い方は想定されていません．[^1]

* `module`内変数はグローバル変数。ではない．private宣言がある．
* モジュール間での名前の衝突，別名の定義
* モジュールに分けると読みやすくなるが、安全にはならない。
* `intent`を使おう。
* あまり依存関係が複雑になりすぎないようにする。
* 関数名のつけかた
* (分割コンパイルの方法。)

---

[^1]: fortranよりも前に，c++やJavaなどのオブジェクト指向言語を学んだ人が見ると，いかにも中途半端です．なぜこんな中途半端なものが作られたのか，というと，FORTRAN77で悪評の高かった`common`文を廃止するためだと思われます．モジュール内変数は，同じモジュールに含まれる関数からは自由に参照できます．module指向という考え方はオブジェクト指向よりも前に提唱され，オブジェクト指向は90年代よりもあとに普及したので，fortran90がモジュールにしか対応していないのはしかたありません．