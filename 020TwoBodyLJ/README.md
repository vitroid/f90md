# 020TwoBodyLJ

2つの質点をつなぐバネの代わりに、Lennard-Jones相互作用を導入します。
対エネルギーの計算と力の計算が多少複雑になります。

## 練習問題 020-1

Lennard-Jones相互作用でも、クーロン力と同様の手順で確認して下さい。
```math
\begin{equation}
    E_p(r) = A/(r^p) - B/(r^q)
\end{equation}
```